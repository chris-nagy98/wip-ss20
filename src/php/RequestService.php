<?php

class RequestService
{
    // returns the contact id of an existing contact with the given email address,
    // if there is any, else returns false
    public static function getExistingContactId($db, $contactAddress)
    {
        $getExistingContacts = "SELECT id
                            FROM contact WHERE email=?";

        $stmt = $db->prepare($getExistingContacts);
        $stmt->execute(array($contactAddress));
        $existingContact = $stmt->fetch();

        if ($existingContact) {

            return $existingContact['id'];
        }

        return false;
    }
}
