<?php


class AlertService
{


    public static function showSimpleErrorAlert($message)
    {
        echo '<div class="alert alert-danger" role="alert">
            ' . $message . '            
            </div>';
    }

    public static function showDismissibleErrorAlert($message)
    {
        echo '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">' . $message . '
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        </div>';
    }

    public static function showDismissibleConfirmationAlert($message)
    {
        echo '
        <div class="alert alert-success alert-dismissible fade show" role="alert">' . $message . '
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        </div>';
    }

    public static function showDismissibleInfoAlert($message)
    {
        echo '
            <div class="alert alert-info">
                ' . $message . '
                <a href="#" class="close" data-dismiss="alert">&times;</a>
            </div>
        ';
    }
}
