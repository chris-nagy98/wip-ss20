<?php

require("User.php");
session_start();

$db = connectDatabase();

// connect to database and store connection in variable db
function connectDatabase()
{
    try {
        $db = new PDO(
            "mysql:host=141.13.8.16;port=3307;dbname=wip20_g7",
            "wip20_g7",
            "cs4uJWHm",
            //set encoding to UTF-8
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            )
        );
    } catch (PDOException $e) {
        die('Verbindung zur Datenbank fehlgeschlagen: ' . $e->getMessage());
    }

    return $db;
}
