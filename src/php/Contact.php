<?php

include 'DateService.php';

class Contact
{
    private $id;
    private $firstname;
    private $lastname;
    private $company;
    private $email;
    private $phone;
    private $lastcontact;

    public function __construct($id, $firstname, $lastname, $company, $email, $phone, $lastcontact)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->company = $company;
        $this->email = $email;
        $this->phone = $phone;
        $this->lastcontact = $lastcontact;
    }

    public function displayCard($role)
    {
        echo '
        <div class="card">
            <div class="card-header form-inline text-bold">
                <h6 class="card-heading">
                    ' . $this->firstname . ' ' . $this->lastname . '
                </h6>';
        // only admin can see edit and delete button
        if ($role == "Admin") {
            echo '
                <div class="card-icons-right">
                    <a type="button" class="card-controls edit-btn" data-toggle="modal" data-target="#edit' . $this->id . '">
                        <svg class="bi bi-pencil edit" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                            <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                        </svg>
                    </a>
                    <a type="button" class="card-controls delete-btn" data-toggle="modal" data-target="#delete' . $this->id . '">
                        <svg class="bi bi-trash" width="1.4em" height="1.4em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                        </svg>
                    </a>
                </div>';
        }
        echo '
            </div>

            <div class="card-body header-row row text-bold">
                <div class="col-md-3">Firma</div>
                <div class="col-md-4">E-Mail</div>
                <div class="col-md-3">Telefon</div>
                <div class="col-md-2">Letzter Kontakt</div>
            </div>
            <div class="card-body content-row row">
                <div class="col-md-3">' . $this->company . '</div>
                <div class="col-md-4">' . $this->email . '</div>
                <div class="col-md-3">' . $this->phone . '</div>
                <div class="col-md-2">' . DateService::formatDate($this->lastcontact) . '</div>
            </div>
        </div>';
    }

    function displayModalEdit()
    {
        echo '
        <div class="modal fade" id="edit' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Kontakt
                                bearbeiten
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                    <div class="modal-body">
                        <form action="kontakt-bearbeiten.php" method="POST">
                        <div class="form-group">
                                <label for="contact-id">Kontakt-ID</label>
                                <input class="form-control" type="text" name="contact-id" required value="' . $this->id . '" readonly>
                            </div>
                            <div class="form-group">
                                <label for="contact-firstname">Vorname</label>
                                <input class="form-control" type="text" name="contact-firstname" required value="' . $this->firstname . '">
                            </div>
                            <div class="form-group">
                                <label for="contact-lastname">Nachname</label>
                                <input class="form-control" type="text" name="contact-lastname" required value="' . $this->lastname . '" >
                            </div>
                            <div class="form-group">
                                <label for="contact-company">Firma</label>
                                <input class="form-control" type="text" name="contact-company" value="' . $this->company . '" required>
                            </div>
                            <div class="form-group">
                                <label for="contact-email">E-Mail</label>
                                <input class="form-control" type="email" name="contact-email" value="' . $this->email . '" required>
                            </div>
                            <div class="form-group">
                                <label for="contact-phone">Telefon</label>
                                <input class="form-control" type="tel" name="contact-phone" placeholder="optional" value="' . $this->phone . '">
                            </div>
                            <div class="form-group">
                                <label for="contact-lastcontact">Letzter
                                    Kontakt</label>
                                <input class="form-control" type="date" name="contact-lastcontact" value="' . $this->lastcontact . '">
                            </div>
                            <button type="submit" class="btn btn-main" name="updateContactBtn">Speichern</button>
                            <button type="button"   class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                        </form>
                    </div>
               </div>
            </div>
        </div>';
    }

    function displayModalDelete()
    {
        echo '
        <div class="modal fade" id="delete' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Kontakt löschen
                        </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="kontakt-loeschen.php" method="post">
                            <div class="form-group">
                                Möchten Sie den Kontakt #<input class ="input-id" name="contact-id" value="' . $this->id . '" readonly><span ><b>' . $this->firstname . ' ' . $this->lastname . '
                                        </b></span>
                                wirklich
                                löschen?
                            </div>
                            <button type="submit" class="btn btn-danger" name="deleteContactBtn">Löschen</button>
                            <button type="button" class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                        </form>

                        

                    </div>
                </div>
            </div>
        </div>';
    }
}
