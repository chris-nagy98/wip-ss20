<?php


class Cv
{
    private $id;
    private $firstname;
    private $lastname;
    private $version;
    private $notes;
    private $language;
    private $location;


    public function __construct($id, $firstname, $lastname, $version, $notes, $language, $location)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->version = $version;
        $this->notes = $notes;
        $this->language = $language;
        $this->location = $location;
    }

    public function displayCard($role)
    {
        echo '
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header form-inline">
                    <a class="show-cv" title="CV ansehen" data-toggle="modal" data-target="#show-cv' . $this->id . '">
                        <h6 class="card-heading">
                            ' . $this->firstname . ' ' . $this->lastname . ' (V ' . $this->version . ')' . '
                        </h6>
                    </a>
    
                    <div class="card-icons-right">';

        // only admin can see edit and delete button
        if ($role == "Admin") {
            echo '
                        <a type="button" class="card-controls edit-btn" data-toggle="modal" data-target="#edit' . $this->id . '">
                            <svg class="bi bi-pencil edit" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                                <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                            </svg>
                        </a>
                        <a type="button" class="card-controls delete-btn" data-toggle="modal" data-target="#delete' . $this->id . '">
                            <svg class="bi bi-trash" width="1.4em" height="1.4em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                            </svg>
                        </a>';
        }
        echo '
                        <a type="button" title="CV ansehen" class="card-controls" data-toggle="modal" data-target="#show-cv' . $this->id . '">
                            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
                                <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="card-body header-row row text-bold">
                    <div class="col-md-3">Sprache</div>
                    <div class="col-md-9">Notizen</div>
                </div>
                <div class="card-body content-row row">
                    <div class="col-md-3">' . $this->language . '</div>
                    <div class="col-md-9">' . $this->notes . '</div>
                </div>
            </div>
        </div>
        ';
    }

    function displayModalEdit()
    {
        echo '
            <div class="modal fade" id="edit' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Lebenslauf
                                bearbeiten
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <form action="cv-bearbeiten.php" method="POST">
                            <div class="form-group">
                                <label for="cv-id">CV-ID</label>
                                <input class="form-control" type="number" name="cv-id" value="' . $this->id . '" readonly>
                            </div>
                            <div class="form-group">
                                <label for="cv-firstname">Vorname</label>
                                <input class="form-control" type="text" name="cv-firstname" value="' . $this->firstname . '" required>
                            </div>
                            <div class="form-group">
                                <label for="cv-lastname">Nachname</label>
                                <input class="form-control" type="text" name="cv-lastname" value="' . $this->lastname . '" required>
                            </div>
                            <div class="form-group">
                                <label for="cv-version">Version</label>
                                <input class="form-control" pattern="^\d*(\.\d{0,1})?$" name="cv-version" value="' . $this->version . '" placeholder="1.0" required>
                            </div>
                            <div class="form-group">
                                <label for="cv-notes">Notizen</label>
                                <input class="form-control" type="text" name="cv-notes"';
        if ($this->notes != NULL) {
            echo 'value="' . $this->notes . '"';
        }
        echo '
                            ></div>
                            <div class="form-group">
                                <label for="cv-language">Sprache</label>
                                <select class="form-control" name="cv-language"   required>
                                    <option value="Deutsch"';
        if ($this->language == "Deutsch") {
            echo 'selected';
        }
        echo '
                                    >Deutsch</option>
                                    <option value="Englisch"';
        if ($this->language == "Englisch") {
            echo 'selected';
        }
        echo '
                                    >Englisch
                                    </option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-main" name="updateCvBtn">Speichern</button>
                            <button type="button"   class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                            </form>
                        </div>
                    </div>
                </div>
        </div>';
    }

    function displayModalDelete()
    {
        echo '
            <div class="modal fade" id="delete' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Lebenslauf löschen
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <form action="cv-loeschen.php" method="post">
                                <div class="form-group">
                                    Möchten Sie den Lebenslauf #<input class="input-id" name="cv-id" value="' . $this->id . '" readonly><br>
                                    von <b>' . $this->firstname . ' ' . $this->lastname, '</b> in der Version 
                                    <b>' . $this->version . '</b> wirklich löschen?
                                </div>
                                <button type="submit" class="btn btn-danger" name="deleteCvBtn">Löschen</button>
                                <button type="button" class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>';
    }

    function displayModalShowCv()
    {
        echo '
            <div class="modal fade" id="show-cv' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="display" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Lebenslauf: ' . $this->firstname . ' ' . $this->lastname . ' (V' . $this->version . ', ' . $this->language . ')
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <embed src="' . $this->location . '" type="application/pdf" width="100%" height="500px"/>
                            <button type="button" class="btn btn-light float-right" data-dismiss="modal">Schließen</button>
                        </div>
                    </div>
                </div>
            </div>';
    }
}
