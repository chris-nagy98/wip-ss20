<?php
require("../php/config.php");

if (isset($_POST['addUserBtn']) && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password-repeat']) && !empty($_POST['role'])) {

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $passwordRepeat = $_POST['password-repeat'];
    $hashedPassword = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $role = $_POST['role'];

    // check if email is already taken by another user
    $stmt = $db->prepare("SELECT * FROM employee WHERE email = ?");
    $stmt->execute(array($email));
    $row = $stmt->fetch();

    if ($row['email'] == $email) {
        $message = "Es existiert bereits ein Nutzer mit der angegebenen E-Mail Adresse.";
        header('location: ../Anfragen/anfragen.php?errormsg=' . $message);

        // verify password
    } else if ($password != $passwordRepeat) {
        echo
            $message = "Die eingegebenen Passwörter stimmen nicht überein.";
        header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
    } else {

        // save new user
        try {
            $sql = "INSERT INTO employee ( firstname, lastname, email, password, role) VALUES (?, ?, ?, ?, ?)";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($firstname, $lastname, $email, $hashedPassword, $role));
            header("location: ../Anfragen/anfragen.php");
        } catch (PDOException $e) {
            $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
            header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
        }
    }
}
