<?php
require '../../vendor/autoload.php';
include 'RequestService.php';

use PhpImap\Exceptions\InvalidParameterException;
use PhpImap\Mailbox;

// IMPORTANT NOTE: IMAP has to be turned on in Settings of Gmail-Account for this to work!



$mailbox = connectMailbox();
set_error_handler("warning_handler");


// connect to Gmail Mailbox via PhpImap and return Mailbox Object
function connectMailbox()
{
    $host = '{imap.gmail.com:993/imap/ssl}INBOX'; // Gmail server and mailbox folder
    $mailUsername = 'wip.ss20.g7@gmail.com'; // Username of Gmail-Account --> E-Mail address
    $mailPassword = 'cs4uJWHm'; // Password of Gmail-Account
    $attachmentsDir = __DIR__; // Directory to save attachments, no need for attachments
    $encoding = 'UTF-8';

    try {
        return new Mailbox($host, $mailUsername, $mailPassword, $attachmentsDir, $encoding);
    } catch (InvalidParameterException $e) {
        $errormsg = "Es konnte keine Verbindung zum E-Mail Postfach hergestellt werden. <br> Bitte hinterlegen Sie ein gültiges E-Mail Postfach!";
        AlertService::showSimpleErrorAlert($errormsg);
        die();
    }
}

// return unseen emails, if there are any, else return false
function getUnseenEmails($mailbox)
{
    try {
        return $mailbox->searchMailbox('UNSEEN');
    } catch (ConnectionException $ex) {
        AlertService::showDismissibleErrorAlert('Verbindung zu Postfach fehlgeschlagen : ' . $ex->getMessage());
        die();
    } catch (Exception $ex) {
        AlertService::showDismissibleErrorAlert('Fehler beim lesen des Postfachs aufgetreten :' . $ex->getMessage());
        die();
    }
}

// saves mails with specified ids to DB via SQL-prepared Statement
function saveMailsToDb($mailIds, $mailbox, $db)
{
    // for each unseen mail: get data and add to db (request or mail)
    foreach ($mailIds as $mailId) {
        $email = $mailbox->getMail(
            $mailId,
            false
        );
        // set values
        $subject = $email->subject;
        // ignore mails from addresses beginning with "no-reply" or "noreply"
        if(substr($email->senderAddress, 0, 8) == "no-reply" ||
            substr($email->senderAddress, 0, 7) == "noreply") {
            continue;
        }
        $sender = $email->senderAddress;
        if ($email->textHtml) {
            $content = utf8_encode($email->textHtml);
        } else {
            $content = utf8_encode($email->textPlain);
        }
        $date = substr($email->date, 0, 10);

        if (getExistingSubjectId($db, $subject) !== false) { // existing subject of a request --> add to history mails
            $recipient = "wip.ss20.g7@gmail.com";
            $request_id = getExistingSubjectId($db, $subject);
            $addToEmails = "INSERT INTO email (request_id, sender, recipient, date, content, subject)
                                VALUES (?, ?, ?, ?, ?, ?)";
            $stmt = $db->prepare($addToEmails);

            $stmt->execute(array($request_id, $sender, $recipient, $date, $content, $subject));
        } else { // new subject --> new request
            // check if contact already exists
            $contact_id = NULL;
            if (RequestService::getExistingContactId($db, $sender) !== false) {
                // save contact_id to the request
                $contact_id = RequestService::getExistingContactId($db, $sender);
            }
            $status = 'Offen'; // new requests are always open at start
            $addToRequests = "INSERT INTO request (subject, sender, content, date, status, contact_id)
                                    VALUES (?, ?, ?, ?, ?, ?);";
            $stmt = $db->prepare($addToRequests);

            $stmt->execute(array($subject, $sender, $content, $date, $status, $contact_id));
        }
        // finally mark every mail as "read"
        $mailbox->markMailAsRead($mailId);

        // set lastContact if contact exists
        if (RequestService::getExistingContactId($db, $sender) !== false) {
            $contact_id = RequestService::getExistingContactId($db, $sender);
            $addLastContact = "UPDATE contact SET lastcontact=? WHERE id=?";
            $contact_id;
            $stmt = $db->prepare($addLastContact);
            $stmt->execute(array($date, $contact_id));
        }
    }
}

function warning_handler($errno, $errstr)
{
    if ($errno == 0 || $errno == 1) {
        echo '';
    }
}

// truncates all common prefixes from given subject string, to make it comparable
function truncateSubject($subject)
{
    if (
        substr($subject, 0, 3) === "RE:" || substr($subject, 0, 3) === "Re:"
        || substr($subject, 0, 3) === "WG:" || substr($subject, 0, 3) === "Wg:"
        || substr($subject, 0, 3) === "AW:" || substr($subject, 0, 3) === "Aw:"
    ) {
        return truncateSubject(substr($subject, 4));
    }
    return $subject;
}

// returns the request id if given subject already exists in a request in the db,
// else returns false
function getExistingSubjectId($db, $subject)
{
    $getAllSubjects = "SELECT subject, id
                        FROM wip20_g7.request;";

    $stmt = $db->prepare($getAllSubjects);
    $stmt->execute();

    while ($existingSubject = $stmt->fetch()) {
        // check if the current existing subject (row) is equal to the given subject
        if (strcmp(truncateSubject($existingSubject['subject']), truncateSubject($subject)) === 0) {
            return $existingSubject['id'];
        }
    }
    return false;
}
