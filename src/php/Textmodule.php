<?php


class Textmodule
{
    private $id;
    private $content;
    private $language;


    public function __construct($id, $content, $language)
    {
        $this->id = $id;
        $this->content = $content;
        $this->language = $language;
    }

    public function displayCard($role)
    {
        echo '
        <div class="col-lg-4 col-md-6">
            <div class="card">';
        if ($role == "Admin") {
            // only admin can see edit and delete button
            echo '
                <div class="card-header d-flex justify-content-end">
                    <a type="button" class="edit-btn mr-3" data-toggle="modal" data-target="#edit' . $this->id . '">
                        <svg class="bi bi-pencil edit" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                            <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                        </svg>
                    </a>
                    <a type="button" class="delete-btn" data-toggle="modal" data-target="#delete' . $this->id . '">
                        <svg class="bi bi-trash" width="1.4em" height="1.4em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                        </svg>
                    </a>
                </div>
                ';
        }
        echo '
                <div class="text-module-content card-body text-wrap text-truncate p-3" title="' . $this->content . '">
                    ' . $this->content . '
                </div>
            </div>
        </div>
        ';
    }

    public function displayModalEdit()
    {
        echo '
                            <!-- Modal (Edit) -->
                            <div class="modal fade" id="edit' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <h4 class="modal-title">Textbaustein
                                                bearbeiten
                                            </h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <div class="modal-body">
                                            <form action="textbaustein-bearbeiten.php" method="POST">
                                            <div class="form-group">
                                                    <label for="textmodule-id">ID</label>
                                                    <input class="form-control" value="' . $this->id . '" name="textmodule-id" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textmodule-content">Text</label>
                                                    <textarea class="form-control" rows="5" name="textmodule-content" required>' . $this->content . '</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textmodule-language">Sprache</label>
                                                    <select class="form-control" name="textmodule-language"   required>
                                                        <option value="Deutsch"';
        if ($this->language == "Deutsch") {
            echo 'selected';
        }
        echo '>Deutsch
                                                        </option>
                                                        <option value="Englisch"';
        if ($this->language == "Englisch") {
            echo 'selected';
        }
        echo '>Englisch
                                                        </option>
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-main" name="updateTextmoduleBtn">Speichern</button>
                                                <button type="button"   class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>';
    }

    public function displayModalDelete()
    {
        echo '
                            <div class="modal fade" id="delete' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
        
                                        <div class="modal-header">
                                            <h4 class="modal-title">Textbaustein löschen
                                            </h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="textbaustein-loeschen.php" method="post">
                                                <div class="form-group">
                                                    Möchten Sie den Textbaustein #
                                                    <input class="input-id" name="textmodule-id" value="' . $this->id . '" readonly>
                                                    wirklich löschen?
                                                </div>
                                                <button type="submit" class="btn btn-danger" name="deleteTextmoduleBtn">Löschen</button>
                                                <button type="button" class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>';
    }
}
