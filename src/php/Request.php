<?php

include 'DateService.php';


class Request
{
    private $id;
    private $subject;
    private $sender;
    private $content;
    private $date;
    private $interview;
    private $result;
    private $favourite;
    private $status;
    private $rejectionReason;
    private $contactId;
    private $charge;
    private $closeDate;
    private $delay;

    public function __construct($id, $subject, $sender, $content, $date, $interview, $result, $favourite, $status, $rejectionReason, $contactId, $charge, $closeDate, $delay)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->sender = $sender;
        $this->content = $content;
        $this->date = $date;
        $this->interview = $interview;
        $this->result = $result;
        $this->favourite = $favourite;
        $this->status = $status;
        $this->rejectionReason = $rejectionReason;
        $this->contactId = $contactId;
        $this->charge = $charge;
        $this->closeDate = $closeDate;
        $this->delay = $delay;
    }

    function displayCard($role)
    {
        // cut subjects that are too long (over 85 chars)
        $subjectToShow = $this->subject;
        if (strlen($this->subject) > 85) {
            $subjectToShow = substr($this->subject, 0, 85) . "...";
        }

        // show "-" for requests where interview is not set
        if ($this->interview == NULL) {
            $interviewToShow = "-";
        } else {
            $interviewToShow = DateService::formatDate($this->interview); // display interview Date in dd.mm.YYYY format
        }

        // check if request is on delay (status is open and request is older than one weak)
        $today = new DateTime(date("d.m.Y"));

        $dateOfRequest = new DateTime(date('d.m.Y', strtotime($this->date)));
        $today->sub(new Dateinterval('P7D'));

        if ($today > $dateOfRequest && $this->status == 'Offen') {
            $delay = TRUE;
        } else {
            $delay = FALSE;
        }

        // display card
        echo '
            <div class="card">
                <div class="card-header form-inline">';
        // only admins are allowed favorize and defavorize requests
        if ($role == "Admin") {
            if ($this->favourite == 0) {
                echo '           
                <a href="anfrage-favorisieren.php?id=' . $this->id . '&favourite=0" type="button" class="card-controls icon-fav" name="favReq">
                    <svg class="bi bi-star" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                    </svg> 
                </a>';
            } else {
                echo '
                <a href="anfrage-favorisieren.php?id=' . $this->id . '&favourite=1" type="button" class="card-controls icon-fav">
                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-star-fill" fill="#8ec546" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                    </svg>
                </a>';
            }
        } else { // for Vievers and Managers disable favorization
            if ($this->favourite == 0) {
                echo '        
                <a type="button" disabled title="Keine Berechtigung" class="card-controls icon-fav" name="favReq">
                    <svg class="bi bi-star" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                    </svg>
                </a>';
            } else {
                echo '
                <a type="button" disabled title="Keine Berechtigung" class="card-controls icon-fav">
                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-star-fill" fill="#8ec546" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                    </svg>
                </a>';
            }
        }
        // show red dot for delayed requests
        if ($delay == TRUE) {

            // show full title in tooltip
            echo '<h6 class="card-heading" title="' . $this->subject . '">';

            echo '<a title="Seit >1 Woche unbeantwortet">
                    <svg width="1em" height="1em"
                    viewBox="0 0 16 16" class="bi bi-circle-fill" fill="#dc3545" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="8" cy="7" r="4"/>
                    </svg>
                </a>';

            // only show cut title in card
            echo '' . $subjectToShow . ' (#' . $this->id . ')
             
            </h6>';

            // show blue dot for new but not delayed requests
        } else if ($delay == FALSE && $this->status == 'Offen') {
            echo '<h6 class="card-heading" title="' . $this->subject . '">';

            echo '<a title="Noch unbeantwortet">
                    <svg width="1em" height="1em"
                    viewBox="0 0 16 16" class="bi bi-circle-fill" fill="#4472c4" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="8" cy="7" r="4"/>
                    </svg>
                </a>';

            echo '' . $subjectToShow . ' (#' . $this->id . ')
            
            </h6>';
        } else {
            echo '
            <h6 class="card-heading" title="' . $this->subject . '">' . $subjectToShow . ' (#' . $this->id . ')</h6>';
        }
        echo '
        <div class="card-icons-right">';
        // only admins can see edit and delete button
        if ($role == "Admin") {
            echo '
                <a type="button" class="card-controls edit-btn" data-toggle="modal" data-target="#edit' . $this->id . '" >
                    <svg class="bi bi-pencil edit" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
                        <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
                    </svg>
                </a>
                <a type="button" class="card-controls delete-btn" data-toggle="modal" data-target="#delete' . $this->id . '">
                    <svg class="bi bi-trash" width="1.4em" height="1.4em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                    </svg>
                </a> ';
        }
        // viewers and managers only see collapse button
        echo '
                <a type="button" class=" card-controls card-collapse-btn" data-toggle="collapse" href="#r' . $this->id . '-expand" role="button" aria-expanded="false" aria-controls="">
                    <svg class="bi bi-chevron-down" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                    </svg>
                </a>
            </div>
        </div>
        <div class="card-body header-row row text-bold justify-content-center">
            <div class="col-md-2">Datum</div>
            <div class="col-md-4">Absender';
        if ($this->contactId == NULL && $role == "Admin") {
            // Notify user, if no contact exists with the sender email
            echo ' 
                <a type="button" class="badge badge-success text-white" data-toggle="modal" title="Neuen Kontakt anlegen" data-target="#saveContact' . $this->id . '">
                    Neu
                </a>';
        }
        echo '            
            </div>
            <div class="col-md-2">Status</div>
            <div class="col-md-2">Interview</div>
            <div class="col-md-2">Ergebnis</div>
        </div>
        <div class="py-3 card-body content-row row justify-content-center">
            <div class="col-md-2">' .
            DateService::formatDate($this->date) . '
            </div>
            <div class="col-md-4">' . $this->sender . '
            </div>
            <div class="col-md-2">' . $this->status . '
            </div>
            <div class="col-md-2">' . $interviewToShow . '
            </div>
            <div class="col-md-2">' . $this->result . '
            </div>
        </div>
            <!-- Collapse content -->
            <div id="r' . $this->id . '-expand" class="card-expanded collapse">
                <div class="card-body">
                    <div class="collapse-content border bg-light p-3 mb-2">
                        ' . $this->content . '
                    </div>
                    <div class="card-content-controls">
                        <div class="float-left">';

        // only admins can send answers to requests
        if ($role == "Admin") {
            echo '
                            <a type="button" href="../Anfrage_beantworten/a-beantworten.php?id=' . $this->id . '" 
                            class="btn btn-main write-answer mr-2">
                                Beantworten
                            </a>';
        }
        // show "display history" button
        echo '
                            <a type="button" class="btn btn-outline-secondary show-history" data-toggle="modal" 
                            data-target="#history-' . $this->id . '">
                                Historie anzeigen
                            </a>
                        </div>
                        <div class="float-right">
                            <a type="button" class="btn btn-light close-content" data-toggle="collapse" href="#r'
            . $this->id . '-expand" role="button" aria-expanded="true">
                                Schließen
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }

    function displayModalEdit($possibleRejectionReasons)
    {
        echo '
        <div class="modal fade" id="edit' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="edit" 
        aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Anfrage #' . $this->id . ' bearbeiten</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="anfrage-bearbeiten.php" method="POST">
                        <div class="form-group">
                            <label for="request-subject">Betreff</label>
                            <input class="form-control" type="text" name="request-subject" 
                            value="' . $this->subject . '" required>
                        </div>
                            <div class="form-group">
                                <label for="request-sender">Absender</label>
                                <input class="form-control" type="email" name="request-sender" 
                                value="' . $this->sender . '" required>
                            </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="request-date">Eingangsdatum</label>
                                    <input class="form-control" type="date" name="request-date" 
                                    value="' . $this->date . '" required>
                                </div>
                                <div class="form-group">
                                    <label for="request-status">Status</label>
                                    <select class="form-control" name="request-status">
                                        <option value="Offen"';
        if ($this->status == "Offen") {
            echo 'selected';
        }
        echo '                      >Offen
                                        </option>
                                        <option value="In Bearbeitung"';
        if ($this->status == "In Bearbeitung") {
            echo 'selected';
        }
        echo '                      >In Bearbeitung
                                        </option>
                                        <option value="Geschlossen"';
        if ($this->status == "Geschlossen") {
            echo 'selected';
        }
        echo '                      >Geschlossen
                                        </option>
                                    </select>
                                </div>';
        if ($this->status == "Geschlossen") {
            echo '
                                    <div class="form-group">
                                    <label for="request-close-date">Abschlussdatum</label>
                                    <input class="form-control" type="date" name="request-close-date" 
                                    value="' . $this->closeDate . '">
                                </div>';
        }
        echo '
                                <div class="form-group">
                                    <label for="request-result">Ergebnis</label>
                                    <select class="form-control" name="request-result" id="result-' . $this->id . '">
                                        <option value="Ausstehend"';
        if ($this->result == "Ausstehend") {
            echo 'selected';
        }
        echo '          id="undetermined-' . $this->id . '">Ausstehend</option>
                                        <option value="Auftrag"';
        if ($this->result == "Auftrag") {
            echo 'selected';
        }
        echo '          id="contract-' . $this->id . '">Auftrag
                                        </option>
                                        <option value="Absage"';
        if ($this->result == "Absage") {
            echo 'selected';
        }
        echo '          id="rejection-' . $this->id . '">Absage
                                        </option>
                                </select>
                            </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="request-id">ID</label>
                                    <input class="form-control" type="number" name="request-id" value="' . $this->id .
            '" readonly>
                                </div>
                                
                                <div class="form-group">
                                    <label for="request-interview-date">Interview</label>
                                    <input class="form-control" type="date" name="request-interview-date"';
        if ($this->interview != NULL) {
            echo 'value="' . $this->interview . '"';
        }
        echo '>
                                </div>
                                
                                                    <div class="form-group" id="hourly-rate-' . $this->id . '">
                        <label for="hourly-rate">Stundensatz</label>
                        <input class="form-control" pattern="^\d*(\.\d{0,2})?$" name="hourly-rate" placeholder="0.00" ';
        if ($this->charge != NULL) {
            echo 'value="' . $this->charge . '"';
        }
        echo $this->charge;
        echo ' >
                        </div>

                            <div class="form-group" id="rejection-reason-' . $this->id . '" >
                                <label for="rejection-reason">Ablehnungsgrund</label>
                                <select class="form-control mb-3" name="rejection-reason">
                                    <option value=""';
        if ($this->rejectionReason == NULL) {
            echo 'selected';
        }
        // display possible rejection reasons as a select
        echo '>Bestehenden Ablehnungsgrund wählen</option>';
        foreach ($possibleRejectionReasons as $rejectionReason) {
            echo ' <option value="' . $rejectionReason . '"';
            if ($this->rejectionReason == $rejectionReason) {
                echo 'selected';
            }
            echo '>' . $rejectionReason . '</option>';
        }
        // offer option to add new rejection reasons
        echo '
                                </select>
                                    <input class="form-control" type="text" name="new-rejection-reason"
                                    placeholder="Neuen Ablehnungsgrund hinzufügen">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-main mr-2" name="updateRequestBtn">Speichern</button>
                    <button type="button" class="btn btn-light " data-dismiss="modal">Abbrechen</button>
                </form>
                </div>
                </div>
            </div>
        </div>';

        // depending on result selected show charge or rejection reason input or none
        $this->getResultOptions();
    }

    function displayModalDelete()
    {
        echo '
        <div class="modal fade" id="delete' . $this->id . '" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Anfrage löschen
                        </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="anfrage-loeschen.php" method="post">
                            <div class="form-group">
                                Möchten Sie die Anfrage #<input class="input-id" name="request-id" readonly value="' . $this->id . '">
                                wirklich
                                löschen?
                            </div>
                            <button type="submit" class="btn btn-danger" name="deleteRequestBtn">Löschen</button>
                            <button type="button" class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>';
    }

    function displayModalHistory($historyMails, $db, $role)
    {
        // get contact data
        $contactData = null;
        if ($this->contactId != NULL) {
            $contactData = $this->getContactData($this->contactId, $db);
            $contactName = $contactData['firstname'] . ' ' . $contactData['lastname'];
            $contactCompany = $contactData['company'];
        }

        echo '
        <div class="modal fade" id="history-' . $this->id . '"
        tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg modal-history">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                        Historie zu Anfrage #' . $this->id . ': ' . $this->subject . '
                        </h5>
                    </div>
                    <div class="modal-body">
                        <ul class="list-group">';
        if ($this->result != "Ausstehend") {
            echo '            <li class="list-group-item text-center">';
            if ($this->result == "Auftrag") {
                echo '            <p>Ergebnis: <strong class="text-success">' . $this->result . '</strong></p>';
                if ($role != "Viewer") {
                    echo '        <p>Angesetzter Stundensatz: ' . number_format($this->charge, 2, ',', '') . '€</p>';
                }
            }
            if ($this->result == "Absage") {
                echo '            <p>Ergebnis: <strong class="text-danger">' . $this->result . '</strong></p>
                                  <p>Ablehnungsgrund: ' . $this->rejectionReason . '</p>';
            }
            echo '           </li>';
        }
        foreach ($historyMails as $historyMail) {
            // get attachments (cvs)
            $attachments = getMailAttachments($db, $historyMail['id']);
            // add all mails to history overview (on left / right side)
            if (strcmp($historyMail['sender'], 'wip.ss20.g7@gmail.com') === 0) {
                $employeeName = "Unbekannt";
                if (isset($historyMail['fname']) && isset($historyMail['lname'])) {
                    $employeeName = $historyMail['fname'] . ' ' . $historyMail['lname'];
                }
                // mail from tool (slidemotif) --> display right
                echo '
                            <li class="list-group-item list-group-item-action">
                                <a class="text-right" data-toggle="collapse" href="#historyContent' . $historyMail['id'] . '"
                                role="button"';
                if ($role != "Viewer") {
                    echo '      title="Inhalt der Mail zeigen">';
                } else {
                    echo '      title="Keine Berechtigung, um Inhalt zu sehen">';
                }
                echo '              <p>Antwort versendet - <strong>' .
                    DateService::formatDate($historyMail['date'])
                    . '</strong></p>
                                    <p>Mitarbeiter: ' . $employeeName . '</p>';
                if (!empty($attachments)) {
                    // display all attached cvs to the mail
                    echo '          <p>Angehängte CVs: ';
                    foreach ($attachments as $attachment) {
                        echo $attachment['fname'] . ' ' . $attachment['lname'] . ' (V' . $attachment['v'] .
                            ', ' . $attachment['l'] . ')<br>';
                    }
                    echo '          </p>';
                }
                echo '          </a>';
                if ($role != "Viewer") {
                    echo '
                                <div class="collapse" id="historyContent' . $historyMail['id'] . '">
                                    <div class="collapse-content border bg-light p-3 mt-2">
                                        ' . $historyMail['content'] . '
                                    </div>
                                </div>
                            </li>
                        ';
                }
            } else {
                // mail from contact/customer --> display left
                echo '
                            <li class="list-group-item list-group-item-action">
                                <a class="text-left" data-toggle="collapse" href="#historyContent' . $historyMail['id'] . '"
                                role="button"';
                if ($role != "Viewer") {
                    echo '      title="Inhalt der Mail zeigen">';
                } else {
                    echo '      title="Keine Berechtigung, um Inhalt zu sehen">';
                }
                echo '              <p>
                                        <strong>' . DateService::formatDate($historyMail['date'])
                    . '</strong> - Antwort eingegangen
                                    </p>
                                    <p>Absender: ' . $historyMail['sender'] . ' </p>
                                </a>';
                if ($role != "Viewer") {
                    echo '
                                </a>
                                <div class="collapse" id="historyContent' . $historyMail['id'] . '">
                                    <div class="collapse-content border bg-light p-3 mt-2">
                                        ' . $historyMail['content'] . '
                                    </div>
                                </div>
                            </li>
                        ';
                }
            }
        }
        echo '               <li class="list-group-item text-left">
                                  <p><strong>' .
            DateService::formatDate($this->date)
            . '</strong> - Anfrage eingegangen</p>
                                  <p>Absender: ' . $this->sender . '</p>';
        if ($contactData != null) {
            echo '              <p>Kontakt: ' . $contactName . ' (' . $contactCompany . ')
                                <p>';
        }
        echo '                  <p>Betreff: ' . $this->subject . '</p>
                            </li>';
        echo '         </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Schließen</button>
                    </div>
                </div>
            </div>
        </div>';
    }

    // Modal to add unknown contacts, sender-email and lastcontact are preselected
    function displayModalSaveContact()
    {
        echo '
        <div class="modal fade" id="saveContact' . $this->id . '" tabindex="-1" role="form" aria-labelledby="import-contact"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Kontakt hinzufügen</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <form action="../Kontakte/kontakt-hinzufuegen.php"
                            method="post">
                            <div class="form-group">
                                <label
                                    for="contact-firstname">Vorname</label>
                                <input class="form-control"
                                    type="text"
                                    name="contact-firstname"
                                    required>
                            </div>
                            <div class="form-group">
                                <label
                                    for="contact-lastname">Nachname</label>
                                <input class="form-control"
                                    type="text"
                                    name="contact-lastname"
                                    required>
                            </div>
                            <div class="form-group">
                                <label
                                    for="contact-company">Firma</label>
                                <input class="form-control"
                                    type="text"
                                    name="contact-company">
                            </div>
                            <div class="form-group">
                                <label
                                    for="contact-email">E-Mail</label>
                                <input class="form-control"
                                    type="email"
                                    name="contact-email" value="' . $this->sender . '">
                            </div>
                            <div class="form-group">
                                <label
                                    for="contact-phone">Telefon</label>
                                <input class="form-control"
                                    type="tel" name="contact-phone"
                                    placeholder="optional">
                            </div>
                            <div class="form-group">
                                <label for="contact-lastcontact">Letzter
                                    Kontakt</label>
                                <input class="form-control" type="date" name="contact-lastcontact" value="' . $this->date . '">
                            </div>

                            <button type="submit"
                                class="btn btn-main"
                                name="addContactBtn">Speichern</button>
                            <button type="button"
                                class="btn btn-light"
                                data-dismiss="modal">Abbrechen</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>';
    }

    // returns the data from the contact that exists with the given contact id
    private function getContactData($contactId, $db)
    {
        $getContacts = "SELECT email, firstname, lastname, company 
                        FROM contact c JOIN request r on c.id = r.contact_id
                        WHERE contact_id = ?";
        $stmt = $db->prepare($getContacts);
        $stmt->execute(array($contactId));

        return $stmt->fetch();
    }

    // hide charge and rejection-reason input when Result is "undetermined", show charge input only when Result
    // "contract" is selected, show rejection-reason input only when Result "rejection" is seleted
    private function getResultOptions()
    {
        echo '
        <script type="text/javascript">
        
        undetermined = document.getElementById("undetermined-' . $this->id . '");
        contract = document.getElementById("contract-' . $this->id . '");
        rejection = document.getElementById("rejection-' . $this->id . '");

        if (undetermined.selected == true) {
            document.getElementById("hourly-rate-' . $this->id . '").style.display = "none";
            document.getElementById("rejection-reason-' . $this->id . '").style.display = "none";
        }

        if (contract.selected == true) {
            document.getElementById("hourly-rate-' . $this->id . '").style.display = "block";
            document.getElementById("rejection-reason-' . $this->id . '").style.display = "none";
        }

        if (rejection.selected == true) {
            document.getElementById("hourly-rate-' . $this->id . '").style.display = "none";
            document.getElementById("rejection-reason-' . $this->id . '").style.display = "block";
        }

        $(document).ready(function () {
            $("#result-' . $this->id . '").change(function () {
                    var optionValue = $("#result-' . $this->id . '").val();
                    if (optionValue == "Ausstehend") {
                        document.getElementById("hourly-rate-' . $this->id . '").style.display = "none";
                        document.getElementById("rejection-reason-' . $this->id . '").style.display = "none";
                    } else if (optionValue == "Auftrag") {
                        document.getElementById("hourly-rate-' . $this->id . '").style.display = "block";
                        document.getElementById("rejection-reason-' . $this->id . '").style.display = "none";
                    } else {
                        document.getElementById("hourly-rate-' . $this->id . '").style.display = "none";
                        document.getElementById("rejection-reason-' . $this->id . '").style.display = "block";
                    }
            }).change();
        });
        </script>';
    }
}
