<?php


class User
{
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $role;

    function __construct($id, $firstname, $lastname, $email, $password, $role)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
    }

    public static function displayModalAddUser()
    {
        echo '
        <div class="modal fade" id="add-user" tabindex="-1" role="dialog" aria-labelledby="add-user" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Neuen Nutzer anlegen
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                
                    <form action="../php/add-user.php" method="post">
                        <div class="form-group">
                            <label for="firstname">Vorname</label>
                            <input class="form-control" type="vchar" name="firstname" required>
                        </div>
                        <div class="form-group">
                            <label for="lastname">Nachname</label>
                            <input class="form-control" type="vchar" name="lastname" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <input class="form-control" type="vchar" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Passwort</label>
                            <input class="form-control" type="password" name="password" required>
                        </div>
                        <div class="form-group">
                            <label for="password-repeat">Passwort wiederholen
                            </label>
                            <input class="form-control" type="password" name="password-repeat" required>
                        </div>
                        <div class="form-group">
                            <label for="role">Rolle</label>
                            <select class="form-control" name="role" id="role" selected="admin" type="enum">
                                <option value="admin">Admin
                                </option>
                                <option value="viewer" selected>Viewer
                                </option>
                                <option value="manager">Manager
                                </option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-main" name="addUserBtn">Speichern</button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">Abbrechen</button>
                    </form>

                </div>
            </div>
        </div>
    </div>';
    }
}
