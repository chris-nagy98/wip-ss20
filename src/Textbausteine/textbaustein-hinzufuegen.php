<?php
require("../php/config.php");

if (isset($_POST['addTextModuleBtn'])  && !empty($_POST['textmodule-text']) && !empty($_POST['textmodule-language'])) {
    $text = $_POST['textmodule-text'];
    $language = $_POST['textmodule-language'];

    try {
        $sql = "INSERT INTO text_module (content, language) VALUES (?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($text, $language));
        header('location: textbausteine.php?confirmationmsg=Der Textbaustein wurde erfolgreich gespeichert!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: textbausteine.php?errormsg=' . $message);
    }
}
