<?php

require("../php/config.php");


if (isset($_POST["updateTextmoduleBtn"]) && !empty($_POST["textmodule-id"]) && !empty($_POST["textmodule-content"]) && !empty($_POST["textmodule-language"])) {

    $id = $_POST["textmodule-id"];
    $update_content = $_POST["textmodule-content"];
    $update_language = $_POST["textmodule-language"];

    try {
        $sql = "UPDATE text_module SET content=?, language=? WHERE id=" . $id;
        $stmt = $db->prepare($sql);
        $stmt->execute(array($update_content, $update_language));
        header('location: textbausteine.php?confirmationmsg=Ihre Änderungen wurden gespeichert!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: textbausteine.php?errormsg=' . $message);
    }
}
