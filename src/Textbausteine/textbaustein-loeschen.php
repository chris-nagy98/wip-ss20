<?php

require("../php/config.php");

if (isset($_POST["deleteTextmoduleBtn"]) && !empty($_POST['textmodule-id'])) {
    $id = $_POST['textmodule-id'];

    try {
        $sql = "DELETE FROM text_module WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        header('location: textbausteine.php?confirmationmsg=Der Textbaustein wurde gelöscht!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: textbausteine.php?errormsg=' . $message);
    }
}
