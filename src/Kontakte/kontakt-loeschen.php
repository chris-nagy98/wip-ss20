<?php

require("../php/config.php");

if (isset($_POST["deleteContactBtn"]) && !empty($_POST['contact-id'])) {
    $id = $_POST['contact-id'];

    try {
        $sql = "DELETE FROM contact WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        header('location: kontakte.php?confirmationmsg=Der Kontakt wurde gelöscht!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: kontakte.php?errormsg=' . $message);
    }
}
