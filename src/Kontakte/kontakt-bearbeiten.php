<?php

require("../php/config.php");


if (isset($_POST["updateContactBtn"]) && !empty($_POST["contact-id"]) && !empty($_POST["contact-firstname"]) && !empty($_POST["contact-lastname"]) && !empty($_POST["contact-email"]) && !empty($_POST["contact-company"])) {

    $id = $_POST["contact-id"];
    $update_firstname = $_POST["contact-firstname"];
    $update_lastname = $_POST["contact-lastname"];
    $update_email = $_POST["contact-email"];
    $update_company = $_POST["contact-company"];
    if (empty($_POST["contact-lastcontact"])) {
        $update_lastcontact = NULL;
    } else {
        $update_lastcontact = $_POST["contact-lastcontact"];
    }
    if (empty($_POST["contact-phone"])) {
        $update_phone = NULL;
    } else {
        $update_phone = $_POST["contact-phone"];
    }

    try {
        $sql = "UPDATE contact SET firstname=?, lastname=?, email=?, company=?, lastcontact=?, phone=? WHERE id=" . $id;
        $stmt = $db->prepare($sql);
        $stmt->execute(array($update_firstname, $update_lastname, $update_email, $update_company, $update_lastcontact, $update_phone));
        header('location: kontakte.php?confirmationmsg=Ihre Änderungen wurden gespeichert!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: kontakte.php?errormsg=' . $message);
    }
}
