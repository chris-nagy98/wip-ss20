<?php
require("../php/config.php");

if (isset($_POST['addContactBtn']) && !empty($_POST['contact-firstname']) && !empty($_POST['contact-lastname']) && !empty($_POST['contact-company']) && !empty($_POST['contact-email'])) {

    $stmt = $db->prepare("SELECT * FROM contact WHERE email = ?");
    $stmt->execute(array($_POST['contact-email']));
    $row = $stmt->fetch();

    // check if there already exists a user with the given email
    if ($row['email'] == $_POST['contact-email']) {
        $errormsg = "Es existiert bereits ein Kontakt mit dieser E-Mail Adresse.";
        header('location: kontakte.php?errormsg=' . $errormsg);
    } else {
        $firstname = $_POST['contact-firstname'];
        $lastname = $_POST['contact-lastname'];
        $company = $_POST['contact-company'];
        $email = $_POST['contact-email'];

        if (empty($_POST['contact-phone'])) {
            $phone = NULL;
        } else {
            $phone = $_POST['contact-phone'];
        }

        if (empty($_POST["contact-lastcontact"])) {
            $lastcontact = NULL;
        } else {
            $lastcontact = $_POST["contact-lastcontact"];
        }

        try {
            $sql = "INSERT INTO contact (email, firstname, lastname, company, phone, lastcontact) VALUES (?, ?, ?, ?, ?, ?)";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($email, $firstname, $lastname, $company, $phone, $lastcontact));

            // set contact Id of request with this sender-email
            $selectId = "SELECT MAX(id) FROM contact";
            $selectStatement = $db->prepare($selectId);
            $selectStatement->execute();
            $cId = $selectStatement->fetch();

            $checkRequest = "SELECT id FROM request WHERE sender = ?";
            $checkStatement = $db->prepare($checkRequest);
            $checkStatement->execute(array($email));

            while ($matchedRequests = $checkStatement->fetch()) {
                $updateRequest = "UPDATE request SET contact_id=? WHERE id=" . $matchedRequests[0];
                $statement = $db->prepare($updateRequest);
                $statement->execute(array($cId[0]));
            }

            header('location: kontakte.php?confirmationmsg=Der Kontakt wurde erfolgreich gespeichert!');
        } catch (PDOException $e) {
            $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
            header('location: kontakte.php?errormsg=' . $message);
        }
    }
}
