<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../vendor/autoload.php';
require '../php/config.php';
include '../php/AlertService.php';
include '../php/RequestService.php';


// check if session variable is set
if (!isset($_SESSION['user'])) {
    header('Location: ../Login/login.php?login=invalid');
}

// Initialize Current User Variable
$currentUser = $_SESSION['user'];

// Send mail
if (isset($_POST["senden"]) && !empty($_POST['to']) && !empty($_POST['subject']) && !empty($_POST['editor'])) {

    // Client-side alert if limit of CVs is exceeded
    if (!empty($_POST["chooseCV"]) && count($_POST["chooseCV"]) > 5) {
        die("Fehler: Es wurden mehr als fünf CVs ausgewählt. Bitte klicken Sie in Ihrem Browser auf 'Zurück' und wählen Sie maximal fünf CVs aus.");
    }

    // set email parameters
    $mail = new PHPMailer(true);
    $mail->IsSMTP();
    $mail->Mailer = "smtp";
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->Username = 'wip.ss20.g7@gmail.com';
    $mail->Password = 'cs4uJWHm';

    $mail->IsHTML(true);
    $mail->AddAddress($_POST['to']);
    if (!empty($_POST['cc'])) {
        $mail->AddCC($_POST["cc"]);
    }
    if (!empty($_POST['bcc'])) {
        $mail->AddBCC($_POST["bcc"]);
    }
    $mail->SetFrom("wip.ss20.g7@gmail.com", "smartRequest");
    $mail->AddReplyTo("wip.ss20.g7@gmail.com", "smartRequest");
    $mail->Subject = $_POST["subject"];

    // attach all selected cvs
    if (!empty("chooseCV") && ($_POST['chooseCV'] != "")) {
        $selectedCv = $_POST['chooseCV'];
        foreach ($selectedCv as $value) {
            $sql = 'SELECT location FROM cv where id = ?';
            $stmt = $db->prepare($sql);
            $stmt->execute(array($value));
            while ($cv = $stmt->fetch()) {
                $location = $cv['location'];
                try {
                    $mail->AddAttachment($location);
                } catch (Exception $e) {
                    $message = "Fehler beim Anhängen des CV";
                    header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
                    exit;
                }
            }
        }
    }

    $mail->isHTML(true);
    $body = $_POST["editor"];
    $mail->Body = $body;

    // send mail
    if (!$mail->send()) {
        $message = "Error while sending Email.";
        header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
        exit;
    }

    // set values for db upload
    $req_id = $_GET['id'];
    $sender = "wip.ss20.g7@gmail.com";
    $recipient = $_POST['to'];
    $sent_date = date('Y-m-d');
    $content = $_POST['editor'];
    $subject = $_POST['subject'];
    $cc = NULL;
    if (!empty($_POST['cc'])) {
        $cc = $_POST['cc'];
    }
    $bcc = NULL;
    if (!empty($_POST['bcc'])) {
        $bcc = $_POST['bcc'];
    }

    //db upload
    try {
        $sql = "INSERT INTO email (request_id, sender, recipient, date, content, subject, cc, bcc, employee_id)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($req_id, $sender, $recipient, $sent_date, $content, $subject, $cc, $bcc, $currentUser->id));
    } catch (PDOException $e) {
        $message = "Etwas ist schiefgelaufen. Bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
        exit;
    }
    // save is_attached_to relation in DB for every attached CV
    try {
        if (!empty($_POST["chooseCV"]) && ($_POST['chooseCV'] != "")) {
            $selectedCv = $_POST['chooseCV'];
            foreach ($selectedCv as $value) {
                $getEmailId = "SELECT MAX(id) FROM email";
                $emailIdStmt = $db->prepare($getEmailId);
                $emailIdStmt->execute();
                while ($emailId = $emailIdStmt->fetch()) {
                    $setAttachedTo = "INSERT INTO is_attached_to (cv_id, email_id) VALUES (?,?)";
                    $attachedStmt = $db->prepare($setAttachedTo);
                    $attachedStmt->execute(array($value, $emailId[0]));
                }
            }
        }
    } catch (PDOException $e) {
        $message = "Etwas ist schiefgelaufen. Bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
        exit;
    }

    // set request status to "in progress"
    try {
        $status = "In Bearbeitung";
        $setStatus = "UPDATE request SET status=? WHERE id=" . $req_id;
        $setStatusStmt = $db->prepare($setStatus);
        $setStatusStmt->execute(array($status));
        header('location: ../Anfragen/anfragen.php?confirmationmsg=E-Mail wurde erfolgreich versendet!');
    } catch (PDOException $e) {
        $message = "Etwas ist schiefgelaufen. Bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: ../Anfragen/anfragen.php?errormsg=' . $message);
        exit;
    }

    // update lastcontact if recipient is existing contact
    if (RequestService::getExistingContactId($db, $recipient) !== false) {
        $lastcontact = date("Y-m-d");
        $contact_id = RequestService::getExistingContactId($db, $recipient);
        $addLastContact = "UPDATE contact SET lastcontact=? WHERE id=?";
        $contact_id;
        $stmt = $db->prepare($addLastContact);
        $stmt->execute(array($lastcontact, $contact_id));
    }
}
?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap 4.5-->
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <!--CKEditor 4-->
    <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <!--JQuery-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/WIP/wip20_g7/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/WIP/wip20_g7/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/WIP/wip20_g7/assets/favicon/favicon-16x16.png">
    <!--Project level styles-->
    <link rel="stylesheet" href="../style.css" type="text/css">
    <?php
    echo '
    <title>Anfrage #' . $_GET['id'] . ' beantworten · SmartRequest · slidemotif AG</title>';
    ?>
</head>

<body>
    <header class="container-fluid">
        <nav class="navbar navbar-light d-flex justify-content-between">
            <div>
                <a class="navbar-brand">
                    <img src="../../assets/img/slidemotif-logo.png" height="55" alt="slidemotif-Logo" loading="lazy">
                </a>
            </div>
            <div id="user-section" class="form-inline d-flex text-white text-center">
                <div class="user-profile d-flex">
                    <svg width="2.5em" height="2.5em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z" />
                        <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                        <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z" />
                    </svg>
                    <div class="user-profile-container ml-3 mr-4 flex-column">
                        <?php
                        echo '
                        <span id="user-email">
                            ' . $currentUser->email . '
                        </span>        
                        <br>
                        <span id="user-role" class="text-uppercase">
                            ' . $currentUser->role . '
                        </span>';
                        ?>
                    </div>
                </div>
                <form method="post" action="a-beantworten.php">
                    <button type="submit" class="navbar-btn btn btn-main" name="logoutBtn">
                        Logout
                    </button>
                </form>
                <?php
                //logout
                if (isset($_POST['logoutBtn'])) {
                    session_destroy();
                    header('Location: ../Login/login.php?logout=success'); //head back to login site
                }
                ?>
            </div>
        </nav>
    </header>
    <!--side menu-->
    <div class="page-with-sidenav">
        <nav class="sidenav">
            <div class="nav-item">
                <a class="nav-link active" href="../Anfragen/anfragen.php" title="Anfragen-Übersicht öffnen">
                    <svg class="bi bi-inboxes" width="2rem" height="2rem" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M.125 11.17A.5.5 0 0 1 .5 11H6a.5.5 0 0 1 .5.5 1.5 1.5 0 0 0 3 0 .5.5 0 0 1 .5-.5h5.5a.5.5 0 0 1 .496.562l-.39 3.124A1.5 1.5 0 0 1 14.117 16H1.883a1.5 1.5 0 0 1-1.489-1.314l-.39-3.124a.5.5 0 0 1 .121-.393zm.941.83l.32 2.562a.5.5 0 0 0 .497.438h12.234a.5.5 0 0 0 .496-.438l.32-2.562H10.45a2.5 2.5 0 0 1-4.9 0H1.066zM3.81.563A1.5 1.5 0 0 1 4.98 0h6.04a1.5 1.5 0 0 1 1.17.563l3.7 4.625a.5.5 0 0 1-.78.624l-3.7-4.624A.5.5 0 0 0 11.02 1H4.98a.5.5 0 0 0-.39.188L.89 5.812a.5.5 0 1 1-.78-.624L3.81.563z" />
                        <path fill-rule="evenodd" d="M.125 5.17A.5.5 0 0 1 .5 5H6a.5.5 0 0 1 .5.5 1.5 1.5 0 0 0 3 0A.5.5 0 0 1 10 5h5.5a.5.5 0 0 1 .496.562l-.39 3.124A1.5 1.5 0 0 1 14.117 10H1.883A1.5 1.5 0 0 1 .394 8.686l-.39-3.124a.5.5 0 0 1 .121-.393zm.941.83l.32 2.562A.5.5 0 0 0 1.884 9h12.234a.5.5 0 0 0 .496-.438L14.933 6H10.45a2.5 2.5 0 0 1-4.9 0H1.066z" />
                    </svg>
                    <br>
                    <span class="nav-text">Anfragen</span>
                </a>
            </div>
            <div class="nav-item">
                <a class="nav-link" href="../Kontakte/kontakte.php" title="Kontakte öffnen">
                    <svg class="bi bi-people" width="2rem" height="2rem" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.995-.944v-.002.002zM7.022 13h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zm7.973.056v-.002.002zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                    </svg>
                    <br>
                    <span class="nav-text">Kontakte</span>
                </a>
            </div>
            <div class="nav-item">
                <a class="nav-link" href="../CVs/cvs.php" title="CV-Übersicht öffnen">
                    <svg class="bi bi-clipboard" width="2rem" height="2rem" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                        <path fill-rule="evenodd" d="M9.5 1h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                    </svg>
                    <br>
                    <span class="nav-text">CVs</span>
                </a>
            </div>
            <div class="nav-item">
                <a class="nav-link" href="../Textbausteine/textbausteine.php" title="Textbausteine-Übersicht öffnen">
                    <svg class="bi bi-textarea-t" width="2rem" height="2rem" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M14 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 1a2 2 0 1 0 0-4 2 2 0 0 0 0 4zM2 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 1a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
                        <path fill-rule="evenodd" d="M1.5 2.5A1.5 1.5 0 0 1 3 1h10a1.5 1.5 0 0 1 1.5 1.5v4h-1v-4A.5.5 0 0 0 13 2H3a.5.5 0 0 0-.5.5v4h-1v-4zm1 7v4a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5v-4h1v4A1.5 1.5 0 0 1 13 15H3a1.5 1.5 0 0 1-1.5-1.5v-4h1z" />
                        <path d="M11.434 4H4.566L4.5 5.994h.386c.21-1.252.612-1.446 2.173-1.495l.343-.011v6.343c0 .537-.116.665-1.049.748V12h3.294v-.421c-.938-.083-1.054-.21-1.054-.748V4.488l.348.01c1.56.05 1.963.244 2.173 1.496h.386L11.434 4z" />
                    </svg>
                    <br>
                    <span class="nav-text">Bausteine</span>
                </a>
            </div>
            <div class="nav-item">
                <a class="nav-link" href="../Reporting/reporting.php" title="Reporting-Übersicht öffnen">
                    <svg class="bi bi-graph-up" width="2rem" height="2rem" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h1v16H0V0zm1 15h15v1H1v-1z" />
                        <path fill-rule="evenodd" d="M14.39 4.312L10.041 9.75 7 6.707l-3.646 3.647-.708-.708L7 5.293 9.959 8.25l3.65-4.563.781.624z" />
                        <path fill-rule="evenodd" d="M10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4h-3.5a.5.5 0 0 1-.5-.5z" />
                    </svg>
                    <br>
                    <span class="nav-text">Reporting</span>
                </a>
            </div>
            <?php
            // show Option to Add New Users only for Admins
            if ($currentUser->role == "Admin") {
                echo
                    '<div class="nav-item mt-auto mb-0">            
                        <a class="button nav-link" role="button" data-toggle="modal" data-target="#add-user" title="Neuen User anlegen">                       
                            <svg width="2rem" height="2rem" viewBox="0 0 16 16" class="bi bi-person-plus mt-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M11 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM1.022 13h9.956a.274.274 0 0 0 .014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 0 0 .022.004zm9.974.056v-.002.002zM6 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zm4.5 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                                <path fill-rule="evenodd" d="M13 7.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
                            </svg>
                            <span class="nav-text m-0">Nutzer anlegen</span>
                        </a>
                    </div>';
            }
            ?>
        </nav>
        <?php
        // Modal Add User
        User::displayModalAddUser();

        // get values from current request via GET-variable
        $req_id = $_GET['id'];
        $sql = "SELECT * FROM request WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($req_id));

        $row = $stmt->fetch();
        $id = $row['id'];
        $subject = $row['subject'];
        $sender = $row['sender'];

        echo '
        <main class="content-wrapper">
            <div class="container-fluid main-container content-wrapper">
                <div class="page-header-container">
                    <h5>Anfrage #' . $req_id . ' beantworten</h5>
                </div>
                <div class="row">
                    <div class="col-8">
                        <form method="post" action="a-beantworten.php?id=' . $req_id . '">
                            <div class="form-group">
                                <label>Empfänger:</label>
                                <input type="email"
                                    class="form-control" value="' . $sender . '" name="to" required>
                            </div>
                            <div class="form-group">
                                <label>Betreff:</label>
                                <input type="text"
                                    class="form-control"
                                    name="subject"
                                    value="RE: ' . $subject . '" required>
                            </div>
                            <div class="form-inline mb-3">
                                <a href="#ccbcc" class="btn btn-outline-secondary mr-3" id="cc_button" 
                                data-toggle="collapse">CC/BCC hinzufügen</a>
                                <a href="#attachcvs" class="btn btn-outline-secondary" id="cv_button" 
                                data-toggle="collapse" title="max. 5">CVs anhängen</a>
                            </div>
                            <div class="collapse" id="ccbcc">
                                <div class="form-group">
                                    <label>CC:</label>
                                    <input type="email"
                                        class="form-control"
                                        name="cc" placeholder="optional"
                                        >
                                </div>
                                <div class="form-group">
                                    <label>BCC:</label>
                                    <input type="email"
                                        class="form-control"
                                        name="bcc" placeholder="optional"
                                        >
                                </div>
                            </div>
                            <div class="form-group collapse" id="attachcvs">
                                <select name="chooseCV[]" class="form-control" id="chooseCV" multiple>';
        // list selectable CVs
        $sql = "SELECT * FROM cv";
        $stmt = $db->prepare($sql);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $cv_id = $row['id'];
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];
            $version = $row['version'];
            $language = $row['language'];
            $location = $row['location'];

            echo '<option value="' . $cv_id . '">' . $firstname . ' ' . $lastname . ' (V' . $version . ', ' . $language . ')</option>';
        }
        echo '
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nachricht:</label>
                                <textarea rows="10"
                                    name="editor" id="answer-message" required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="senden" class="btn btn-main"
                                style="float: right;" data-toggle="modal">
                                Senden
                                </button>
                                <button type="button" class="btn btn-light"
                                data-toggle="modal" data-target="#discard">
                                Zurück
                                </button>
                            </div>
                        </form>
                    </div>
                    <!--Text Modules-->
                    <div class="col-4">
                        <form method="post" action="a-beantworten.php?id=' . $req_id . '">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select class="form-control" name="textmodule-language" required>
                                            <option value = "Alle">Alle</option>
                                            <option value = "Deutsch">Deutsch</option>
                                            <option value = "Englisch">Englisch</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-main" name="filterLanguageBtn">Filtern</button>
                                </div>
                             </div>
                        </form>
                        <div class="card text-modules">
                            <ul class="list-group list-group-flush">';

        // if filter is set display filtered textmodules
        if (isset($_POST['filterLanguageBtn']) && !empty($_POST['textmodule-language']) && ($_POST['textmodule-language'] != "Alle")) {
            $language = $_POST['textmodule-language'];
            $getTextmodules = 'SELECT * FROM text_module WHERE language = ?';

            $resTextmodules = $db->prepare($getTextmodules);
            $resTextmodules->execute(array($language));

            while ($rowTextmodules = $resTextmodules->fetch()) {
                echo '<li class="list-group-item list-group-item-action" title="Textbaustein einfügen">'
                    . $rowTextmodules['content'] .
                    '</li>';
            }
            echo '
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </main>';
        } else {
            // else display all textmodules
            $getTextmodules = "SELECT * FROM text_module ORDER BY id;";
            $resTextmodules = $db->prepare($getTextmodules);
            $resTextmodules->execute();

            while ($rowTextmodules = $resTextmodules->fetch()) {
                echo '<li class="list-group-item">'
                    . $rowTextmodules['content'] .
                    '</li>';
            }
            echo '
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </main>';
        }
        ?>
    </div>

    <!-- Modal Discard -->
    <div class="modal fade" id="discard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Entwurf
                        löschen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Wollen Sie den Entwurf wirklich löschen und zurück zur
                    Anfrage?
                </div>
                <div class="modal-footer">
                    <a href="../Anfragen/anfragen.php">
                        <button class="btn btn-danger">Löschen
                        </button>
                    </a>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Abbrechen
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!--Bootstrap 4.5 resources-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <!--Project-level Scripts-->
    <script src="a-beantworten.js" type="application/javascript"></script>
</body>

</html>