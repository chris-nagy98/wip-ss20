// dynamic use of text modules

let editor = CKEDITOR.replace("editor");

let textmodules = document.getElementsByClassName('list-group-item');
let textArray = [];

for (let j = 0; j < textmodules.length; j++) {
    textArray.push(textmodules[j].innerHTML);
}
for (let i = 0; i < textmodules.length; i++) {
    textmodules[i].addEventListener("click", callback(textArray[i]), false);
}

function callback(text) {
    return function () {
        let data = editor.getData();
        editor.setData(data + text);
    }
}

// maximum 5 CVs selected

let last_valid_selection = null;
$('#chooseCV').change(function () {
    if ($(this).val().length > 5) {
        $(this).val(last_valid_selection);
    } else {
        last_valid_selection = $(this).val();
    }
});