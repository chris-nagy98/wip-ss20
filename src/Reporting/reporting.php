<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap 4.5-->
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/WIP/wip20_g7/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/WIP/wip20_g7/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/WIP/wip20_g7/assets/favicon/favicon-16x16.png">
    <!--Project level styles-->
    <link rel="stylesheet" href="../style.css" type="text/css">
    <link rel="stylesheet" href="reporting.css" type="text/css">
    <title>Reporting · SmartRequest · slidemotif AG</title>
</head>

<body>
<?php
require("../php/config.php"); ?>

<?php
//check if session variable is set
if (!isset($_SESSION['user'])) {
    header('Location: ../Login/login.php?login=invalid');
}

// Initialize Current User Variable
$currentUser = $_SESSION['user'];
?>

<header class="container-fluid">
    <nav class="navbar navbar-light d-flex justify-content-between">
        <div>
            <a class="navbar-brand">
                <img src="../../assets/img/slidemotif-logo.png" height="55"
                     alt="slidemotif-Logo" loading="lazy">
            </a>
        </div>
        <div id="user-section" class="form-inline d-flex text-white text-center">
            <div class="user-profile d-flex">
                <svg width="2.5em" height="2.5em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
                    <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
                </svg>
                <div class="user-profile-container ml-3 mr-4 flex-column">
                    <?php
                    echo '
                        <span id="user-email">
                            ' . $currentUser->email . '
                        </span>        
                        <br>
                        <span id="user-role" class="text-uppercase">
                            ' . $currentUser->role . '
                        </span>';
                    ?>
                </div>
            </div>
            <form method="post" action="reporting.php">
                <button type="submit" class="navbar-btn btn btn-main"
                        name="logoutBtn">
                    Logout
                </button>
            </form>
            <?php
            //logout
            if (isset($_POST['logoutBtn'])) {
                session_destroy();
                header('Location: ../Login/login.php?logout=success'); //head back to login site
            }
            ?>
        </div>
    </nav>
</header>

<!--sidemenu-->
<div class="page-with-sidenav">
    <nav class="sidenav">
        <div class="nav-item">
            <a class="nav-link" href="../Anfragen/anfragen.php"
               title="Anfragen-Übersicht öffnen">
                <svg class="bi bi-inboxes" width="2rem" height="2rem"
                     viewBox="0 0 16 16" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M.125 11.17A.5.5 0 0 1 .5 11H6a.5.5 0 0 1 .5.5 1.5 1.5 0 0 0 3 0 .5.5 0 0 1 .5-.5h5.5a.5.5 0 0 1 .496.562l-.39 3.124A1.5 1.5 0 0 1 14.117 16H1.883a1.5 1.5 0 0 1-1.489-1.314l-.39-3.124a.5.5 0 0 1 .121-.393zm.941.83l.32 2.562a.5.5 0 0 0 .497.438h12.234a.5.5 0 0 0 .496-.438l.32-2.562H10.45a2.5 2.5 0 0 1-4.9 0H1.066zM3.81.563A1.5 1.5 0 0 1 4.98 0h6.04a1.5 1.5 0 0 1 1.17.563l3.7 4.625a.5.5 0 0 1-.78.624l-3.7-4.624A.5.5 0 0 0 11.02 1H4.98a.5.5 0 0 0-.39.188L.89 5.812a.5.5 0 1 1-.78-.624L3.81.563z"/>
                    <path fill-rule="evenodd"
                          d="M.125 5.17A.5.5 0 0 1 .5 5H6a.5.5 0 0 1 .5.5 1.5 1.5 0 0 0 3 0A.5.5 0 0 1 10 5h5.5a.5.5 0 0 1 .496.562l-.39 3.124A1.5 1.5 0 0 1 14.117 10H1.883A1.5 1.5 0 0 1 .394 8.686l-.39-3.124a.5.5 0 0 1 .121-.393zm.941.83l.32 2.562A.5.5 0 0 0 1.884 9h12.234a.5.5 0 0 0 .496-.438L14.933 6H10.45a2.5 2.5 0 0 1-4.9 0H1.066z"/>
                </svg>
                <br>
                <span class="nav-text">Anfragen</span>
            </a>
        </div>
        <div class="nav-item">
            <a class="nav-link" href="../Kontakte/kontakte.php"
               title="Kontakte öffnen">
                <svg class="bi bi-people" width="2rem" height="2rem"
                     viewBox="0 0 16 16" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.995-.944v-.002.002zM7.022 13h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zm7.973.056v-.002.002zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
                </svg>
                <br>
                <span class="nav-text">Kontakte</span>
            </a>
        </div>
        <div class="nav-item">
            <a class="nav-link" href="../CVs/cvs.php" title="CV-Übersicht öffnen">
                <svg class="bi bi-clipboard" width="2rem" height="2rem"
                     viewBox="0 0 16 16" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
                    <path fill-rule="evenodd"
                          d="M9.5 1h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
                </svg>
                <br>
                <span class="nav-text">CVs</span>
            </a>
        </div>
        <div class="nav-item">
            <a class="nav-link" href="../Textbausteine/textbausteine.php" title="Textbausteine-Übersicht öffnen">
                <svg class="bi bi-textarea-t" width="2rem" height="2rem"
                     viewBox="0 0 16 16" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M14 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 1a2 2 0 1 0 0-4 2 2 0 0 0 0 4zM2 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 1a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/>
                    <path fill-rule="evenodd"
                          d="M1.5 2.5A1.5 1.5 0 0 1 3 1h10a1.5 1.5 0 0 1 1.5 1.5v4h-1v-4A.5.5 0 0 0 13 2H3a.5.5 0 0 0-.5.5v4h-1v-4zm1 7v4a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5v-4h1v4A1.5 1.5 0 0 1 13 15H3a1.5 1.5 0 0 1-1.5-1.5v-4h1z"/>
                    <path
                            d="M11.434 4H4.566L4.5 5.994h.386c.21-1.252.612-1.446 2.173-1.495l.343-.011v6.343c0 .537-.116.665-1.049.748V12h3.294v-.421c-.938-.083-1.054-.21-1.054-.748V4.488l.348.01c1.56.05 1.963.244 2.173 1.496h.386L11.434 4z"/>
                </svg>
                <br>
                <span class="nav-text">Bausteine</span>
            </a>
        </div>
        <div class="nav-item">
            <a class="nav-link active" href="../Reporting/reporting.php" title="Reporting-Übersicht öffnen">
                <svg class="bi bi-graph-up" width="2rem" height="2rem"
                     viewBox="0 0 16 16" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h1v16H0V0zm1 15h15v1H1v-1z"/>
                    <path fill-rule="evenodd"
                          d="M14.39 4.312L10.041 9.75 7 6.707l-3.646 3.647-.708-.708L7 5.293 9.959 8.25l3.65-4.563.781.624z"/>
                    <path fill-rule="evenodd"
                          d="M10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4h-3.5a.5.5 0 0 1-.5-.5z"/>
                </svg>
                <br>
                <span class="nav-text">Reporting</span>
            </a>
        </div>
        <?php
        // show Option to Add New Users only for Admins
        if ($currentUser->role == "Admin") {
            echo
            '<div class="nav-item mt-auto mb-0">            
                        <a class="button nav-link" role="button" data-toggle="modal" data-placement="right" data-target="#add-user" title="Neuen User anlegen">                       
                            <svg width="2rem" height="2rem" viewBox="0 0 16 16" class="bi bi-person-plus mt-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M11 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM1.022 13h9.956a.274.274 0 0 0 .014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 0 0 .022.004zm9.974.056v-.002.002zM6 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zm4.5 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                                <path fill-rule="evenodd" d="M13 7.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
                            </svg>
                            <span class="nav-text m-0">Nutzer anlegen</span>
                        </a>
                    </div>';
        }
        ?>
    </nav>

    <main class="content-wrapper">
        <div class="container-fluid main-container content-wrapper">
            <div class="main-content-container pt-3">
                <?php
                require 'reportingData.php';
                ?>
                <!-- 1st Row -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card border-0">
                            <h5 class="card-title">
                                Statusübersicht Anfragen
                            </h5>
                            <div id="piechart-status"></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card border-0">
                            <h5 class="card-title">
                                Häufigste Ablehnungsgründe
                            </h5>
                            <div id="colchart-reasons"></div>
                        </div>
                    </div>
                </div>
                <!-- 2nd Row -->
                <div class="row mb-2">
                    <div class="col-lg-6">
                        <div class="card border-0">
                            <h5 class="card-title">
                                Neue Anfragen <small
                                        class="text-muted">(letzte 4
                                    Monate)</small>
                            </h5>
                            <div id="barchart-requests"></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card border-0">
                            <h5 class="card-title">
                                Durchschnittliche Durchlaufzeit
                            </h5>
                            <div id="avg-lead-time" class="text-center">
                                <h1 class="my-auto display-1"><?php echo getAvgLeadTime($db) ?> Tage</h1>
                            </div>
                        </div>
                    </div>
                </div>


                <script>
                    window.onload = () => {
                        CanvasJS.addColorSet("slidemotifColors",
                            [
                                "#4472C4",
                                "#8EC546",
                                "#008080",
                                "#2E8B57",
                                "#3CB371",
                                "#90EE90"
                            ]);

                        // Piechart: Current requests and their status
                        let pieChartStatus = new CanvasJS.Chart(
                            "piechart-status", {
                                animationEnabled: true,
                                theme: "light2",
                                colorSet: "slidemotifColors",
                                data: [{
                                    type: "pie",
                                    indexLabel: "{label}",
                                    indexLabelPlacement: "outside",
                                    indexLabelFontColor: "grey",
                                    indexLabelFontSize: 13,
                                    dataPoints: <?php echo
                                    getRequestStatus($db)?>
                                }]
                            });
                        pieChartStatus.render();

                        // Columnchart: Most frequent rejection-reasons
                        let colChartReason = new CanvasJS.Chart(
                            "colchart-reasons", {
                                animationEnabled: true,
                                theme: "light2",
                                colorSet: "slidemotifColors",
                                axisY: {
                                    gridThickness: 0
                                },
                                data: [{
                                    type: "column",
                                    indexLabel: "{y}",
                                    indexLabelFontSize: 13,
                                    dataPoints: <?php echo getFrequentRejectionReasons($db)?>
                                }]
                            });
                        colChartReason.render();

                        // Barchart: Recent number of requests per month (last 4)
                        let barChartRequests = new CanvasJS.Chart(
                            "barchart-requests", {
                                animationEnabled: true,
                                theme: "light2",
                                colorSet: "slidemotifColors",
                                axisY: {
                                    gridThickness: 0
                                },
                                data: [{
                                    type: "bar",
                                    indexLabel: "{y}",
                                    dataPoints: <?php echo getRecentNumberOfRequests($db)?>
                                }]
                            });
                        barChartRequests.render();
                    }
                </script>

                <?php
                // Modal Add User
                $currentUser->displayModalAddUser();
                ?>

            </div>
    </main>
</div>

<!--Bootstrap 4.5 ressources-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
<!--------CanvasJS------------>
<script src="../../assets/script/canvasjs.min.js"></script>
</body>

</html>