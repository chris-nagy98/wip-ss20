<?php

// get number of requests for each status
function getRequestStatus($db)
{
    $stmt = $db->prepare("SELECT request.status, COUNT(*) as y
                        FROM request
                        GROUP BY request.status");
    $stmt->execute();
    $data = array();

    while ($row = $stmt->fetch()) {
        array_push($data, array(
            "label" => $row['status'], "y" => $row['y']
        ));
    }

    return json_encode($data, JSON_NUMERIC_CHECK);
}

// get number of requests that have been rejected ordered by rejection reason
function getFrequentRejectionReasons($db)
{
    $stmt = $db->prepare("SELECT request.reason_rejection, COUNT(reason_rejection) as y
                        FROM request
                        WHERE reason_rejection IS NOT NULL
                        GROUP BY request.reason_rejection
                        ORDER BY y");
    $stmt->execute();
    $data = array();

    while ($row = $stmt->fetch()) {
        array_push($data, array(
            "label" => $row['reason_rejection'], "y" => $row['y']
        ));
    }

    return json_encode($data, JSON_NUMERIC_CHECK);
}

// get total amount of requests that came in per month (last 4)
function getRecentNumberOfRequests($db)
{
    $stmt = $db->prepare("SELECT YEAR(date) as year, MONTH(date) as month, COUNT(*) as numberOfRequests
                        FROM request
                        WHERE MONTH(date) <= MONTH(CURRENT_DATE())
                        GROUP BY year, month
                        LIMIT 4");
    $stmt->execute();
    $data = array();

    while ($row = $stmt->fetch()) {
        array_push($data, array(
            "label" => $row['month'] . '/' . $row['year'], "y" => $row['numberOfRequests']
        ));
    }

    return json_encode($data, JSON_NUMERIC_CHECK);
}

// get average time between date of receipt and close date of all closed reqests
function getAvgLeadTime($db)
{
    $getRequests = "SELECT date, close_date
                    FROM request
                    WHERE close_date IS NOT NULL";
    $stmt = $db->prepare($getRequests);
    $stmt->execute();

    $totalLeadTime = 0;

    // sum of lead time of all closed requests
    while ($request = $stmt->fetch()) {
        $startDate = date_create($request['date']);
        $endDate = date_create($request['close_date']);
        $interval = date_diff($endDate, $startDate);
        $totalLeadTime = $totalLeadTime + $interval->days;
    }

    // number of closed requests
    $getAmountOfClosedRequests = $db->prepare("SELECT COUNT(close_date) FROM request WHERE close_date IS NOT NULL");
    $getAmountOfClosedRequests->execute();
    $amountOfClosedRequests = $getAmountOfClosedRequests->fetch();

    // average lead time
    return intdiv($totalLeadTime, $amountOfClosedRequests[0]);
}
