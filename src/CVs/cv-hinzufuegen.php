    <?php
    require("../php/config.php");


    if (isset($_POST['addCvBtn']) && !empty($_POST['cv-firstname']) && !empty($_POST['cv-lastname']) && !empty($_POST['cv-version']) && !empty($_POST['cv-language'])) {

        $firstname = $_POST['cv-firstname'];
        $lastname = $_POST['cv-lastname'];
        $version = $_POST['cv-version'];
        $language = $_POST['cv-language'];
        if (empty($_POST['cv-notes'])) {
            $notes = NULL;
        } else {
            $notes = $_POST['cv-notes'];
        }


        // get file name and create target path
        if (($_FILES['cv-file']['name'] != "")) {
            $target_dir = "../../assets/cv/";
            $file = $_FILES['cv-file']['name'];
            $path = pathinfo($file);
            $ext = $path['extension'];
            $temp_name = $_FILES['cv-file']['tmp_name'];
            $path_filename_ext = $target_dir . $firstname . "_" . $lastname . "_" . $version . "." . $ext;


            // only allow PDF Files

            if ($ext != "pdf") {
                $message = "Falscher Dateityp, CV konnte nicht gespeichert werden. <br> Bitte wählen Sie eine PDF-Datei!";
                header('location: cvs.php?errormsg=' . $message);
            }

            // Check if file already exists
            else if (file_exists($path_filename_ext)) {
                $message = "CV konnte nicht gespeichert werden. <br> Es exisitert bereits ein CV von " . $firstname . " " . $lastname . " Version " . $version . "<br> Bitte Version anpassen!";
                header('location: cvs.php?errormsg=' . $message);
            } else {

                // Save file in assets/cv folder
                move_uploaded_file($temp_name, $path_filename_ext);

                // Save CV-Data in Database
                $sql = "INSERT INTO cv (firstname, lastname, version, language, notes, location) VALUES (?, ?, ?, ?, ?, ?)";
                $stmt = $db->prepare($sql);
                $stmt->execute(array($firstname, $lastname, $version, $language, $notes, $path_filename_ext));
                header('location: cvs.php?confirmationmsg=Der CV wurde erfolgreich gespeichert!');
            }
        } else {
            $message = "Datei konnte nicht hochgeladen werden. Bitte versuchen Sie es erneut.";
            header('location: cvs.php?errormsg=' . $message);
        }
    }




    ?>