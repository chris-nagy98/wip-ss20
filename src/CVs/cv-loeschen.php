<?php

require("../php/config.php");

if (isset($_POST["deleteCvBtn"]) && !empty($_POST['cv-id'])) {

    $id = $_POST['cv-id'];
    $locationSQL = "SELECT location FROM cv WHERE id=?";
    $locationSTMT = $db->prepare($locationSQL);
    $locationSTMT->execute(array($id));
    while ($row = $locationSTMT->fetch()) {
        $location = $row['location'];

        // delete file from assets/cv folder
        try {
            unlink($location);
        } catch (Exception $e) {
            $message = "Datei konnte nicht gelöscht werden, bitte erneut versuchen! <br>" . $e->getMessage();
            header('location: cvs.php?errormsg=' . $message);
        }
    }

    // delete database entry
    try {
        $sql = "DELETE FROM cv WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        header('location: cvs.php?confirmationmsg=Der CV wurde gelöscht!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: cvs.php?errormsg=' . $message);
    }
}
