<?php

require("../php/config.php");


if (isset($_POST["updateCvBtn"]) && !empty($_POST["cv-id"]) && !empty($_POST["cv-firstname"]) && !empty($_POST["cv-lastname"]) && !empty($_POST["cv-version"]) && !empty($_POST["cv-language"])) {

    $id = $_POST["cv-id"];
    $update_firstname = $_POST["cv-firstname"];
    $update_lastname = $_POST["cv-lastname"];
    $update_version = $_POST["cv-version"];
    $update_language = $_POST["cv-language"];
    if (empty($_POST["cv-notes"])) {
        $update_notes = NULL;
    } else {
        $update_notes = $_POST["cv-notes"];
    }

    try {
        $sql = "UPDATE cv SET firstname=?, lastname=?, version=?, language=?, notes=? WHERE id=" . $id;
        $stmt = $db->prepare($sql);
        $stmt->execute(array($update_firstname, $update_lastname, $update_version, $update_language, $update_notes));
        header('location: cvs.php?confirmationmsg=Ihre Änderungen wurden gespeichert!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: cvs.php?errormsg=' . $message);
    }
}
