<?php

require("../php/config.php");

if (isset($_POST["deleteRequestBtn"]) && !empty($_POST['request-id'])) {
    $id = $_POST['request-id'];

    try {
        $sql = "DELETE FROM request WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        header('location: anfragen.php?confirmationmsg=Die Anfrage wurde gelöscht!');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: anfragen.php?errormsg=' . $message);
    }
}
