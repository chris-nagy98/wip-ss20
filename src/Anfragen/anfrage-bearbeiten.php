<?php

require("../php/config.php");


if (isset($_POST["updateRequestBtn"]) && !empty($_POST["request-id"]) && !empty($_POST["request-date"]) && !empty($_POST["request-subject"]) && !empty($_POST["request-status"]) && !empty($_POST["request-sender"]) && !empty($_POST["request-result"])) {

    $id = $_POST["request-id"];
    $update_date = $_POST["request-date"];
    $update_subject = $_POST["request-subject"];
    $update_sender = $_POST["request-sender"];
    $update_status = $_POST["request-status"];
    $update_result = $_POST["request-result"];
    if (empty($_POST["request-interview-date"])) {
        $update_interview = NULL;
    } else {
        $update_interview = $_POST["request-interview-date"];
    }
    if (empty($_POST["hourly-rate"])) {
        $update_charge = NULL;
    } else {
        $update_charge = $_POST["hourly-rate"];
    }


    // get status before update
    $getStatus = "SELECT status FROM request
                            WHERE id=" . $id;
    $resStatus = $db->prepare($getStatus);
    $resStatus->execute();
    $statusBeforeUpdate = $resStatus->fetch();

    // get result before update
    $getResult = "SELECT result FROM request
                            WHERE id=" . $id;
    $resResult = $db->prepare($getResult);
    $resResult->execute();
    $resultBeforeUpdate = $resResult->fetch();

    // get close_date before update
    $getCloseDate = "SELECT close_date FROM request
        WHERE id=" . $id;
    $resCloseDate = $db->prepare($getCloseDate);
    $resCloseDate->execute();
    $closeDateBeforeUpdate = $resCloseDate->fetch();


    // close request automatically if result is set to "contract" or "no contract"
    if (($resultBeforeUpdate[0] == "Ausstehend") && ($update_result != "Ausstehend")) {
        $update_status = "Geschlossen";
    }

    // set close date if request is closed
    if (($statusBeforeUpdate[0] != "Geschlossen") && ($update_status == "Geschlossen")) {
        $close_date = date("Y-m-d");

        // if close date is set manually for already closed request update close date
    } else if (!empty($_POST['request-close-date'])) {
        $close_date = $_POST['request-close-date'];

        // else closed date remains the same or NULL
    } else {
        $close_date = $closeDateBeforeUpdate[0];
    }

    // check if rejection reason is selected or a new one needs to be created
    if (($_POST["rejection-reason"] != "") && (!empty($_POST["new-rejection-reason"]))) {
        $message = "Es darf nur ein Ablehnungsgrund ausgewählt werden, Änderungen konnten nicht gespeichert werden";
        header('location: anfragen.php?errormsg=' . $message);
        die();
    } else if (($_POST["rejection-reason"] == "") && (empty($_POST["new-rejection-reason"]))) {
        $update_reason_rejection = NULL;
    } else if (($_POST["rejection-reason"] != "") && (empty($_POST["new-rejection-reason"]))) {
        $update_reason_rejection = $_POST["rejection-reason"];
    } else if (($_POST["rejection-reason"] == "") && (!empty($_POST["new-rejection-reason"]))) {
        $update_reason_rejection = $_POST["new-rejection-reason"];
        // get enum values of rejection reasons
        $getReasons = "SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE TABLE_NAME = 'request' 
                            AND COLUMN_NAME = 'reason_rejection'";
        $result = $db->prepare($getReasons);
        $result->execute(array());
        $row = $result->fetch();
        $reasons_array = explode(",", str_replace("'", "", substr($row['COLUMN_TYPE'], 5, (strlen($row['COLUMN_TYPE']) - 6))));

        // create string 
        $reasons_string = "";
        foreach ($reasons_array as $key => $val) {
            $reasons_string = $reasons_string . "'" . $val . "',";
        }
        $reasons_string = $reasons_string . "'" . $update_reason_rejection . "'";

        // add new rejection reason to DB

        try {
            $addReason = "ALTER TABLE request CHANGE reason_rejection reason_rejection ENUM(" . $reasons_string . ") CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL";
            $reasonStmnt = $db->prepare($addReason);
            $reasonStmnt->execute();
        } catch (PDOException $e) {
            $message = "Neuer Ablehnungsgrund konnte nicht gespeichert werden. Bitte erneut versuchen " . $e->getMessage();
            header('location = anfragen.php?errormsg=' . $message);
        }
    }
    //update request in DB
    try {
        $sql = "UPDATE request SET subject=?, sender=?, date=?, interview=?, result=?, status=?, reason_rejection=?, charge=?, close_date =? WHERE id=" . $id;
        $stmt = $db->prepare($sql);
        $stmt->execute(array($update_subject, $update_sender, $update_date, $update_interview, $update_result, $update_status, $update_reason_rejection, $update_charge, $close_date));
        header('location: anfragen.php?confirmationmsg=Ihre Änderungen wurden gespeichert');
    } catch (PDOException $e) {
        $message = "Etwas ist schief gelaufen, bitte erneut versuchen! <br>" . $e->getMessage();
        header('location: anfragen.php?errormsg=' . $message);
    }
}
