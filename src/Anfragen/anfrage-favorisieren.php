<?php

require("../php/config.php");

if (isset($_GET['id']) && isset($_GET['favourite'])) {

    $id = $_GET['id'];
    $favourite = $_GET['favourite'];

    if ($favourite == 0) {
        $update_favourite = 1;
    } else {
        $update_favourite = 0;
    }

    $sql = "UPDATE request SET favourite=? WHERE id=" . $id;
    $stmt = $db->prepare($sql);
    $stmt->execute(array($update_favourite));
    header('location: anfragen.php');
}
