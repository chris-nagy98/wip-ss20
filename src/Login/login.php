<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--Bootstrap 4.5-->
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <!--Fontawesome Icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/503fd82dc5.js" crossorigin="anonymous"></script>
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/WIP/wip20_g7/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/WIP/wip20_g7/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/WIP/wip20_g7/assets/favicon/favicon-16x16.png">
    <!--Project level styles-->
    <link rel="stylesheet" href="../style.css" type="text/css">
    <link href="login.css" type="text/css" rel="stylesheet">
    <title>Login · SmartRequest · slidemotif AG</title>
</head>

<body>
    <?php
    require("../php/config.php");
    require("../php/AlertService.php");
    ?>

    <div class="container">
        <div class="login-container ">
            <div class="alert-container text-center">
                <?php
                //show Alerts
                if (isset($_GET['errormsg'])) {
                    AlertService::showDismissibleErrorAlert($_GET['errormsg']);
                }
                ?>
            </div>
            <form class="login-form" method="POST" action="login.php">
                <div class="form-group form-heading text-center mb-4">
                    <img src="../../assets/img/slidemotif-logo.png" alt="slidemotif AG Logo">
                    <span id="tool-description" class="font-main">
                        SmartRequest
                    </span>
                </div>

                <div class="input-icon form-group mb-4">
                    <label for="user-email">E-Mail</label>
                    <input type="email" class="form-control" id="user-email" name="user-email" required>
                    <i class="fa fa-user"></i>
                </div>

                <div class="input-icon form-group mb-4">
                    <label for="user-password">Passwort</label>
                    <input type="password" class="form-control" id="user-password" name="password" required>
                    <i class="fa fa-lock"></i>
                </div>
                <button type="submit" class="btn btn-main btn-block mx-auto w-50" name="loginBtn">
                    Login
                </button>
            </form>

        </div>

    </div>

    <?php
    // functionality of login
    if (isset($_POST['loginBtn']) && !empty($_POST['user-email']) && !empty($_POST['password'])) {
        $email = $_POST['user-email'];
        $password = $_POST['password'];

        $stmt = $db->prepare("SELECT * FROM employee WHERE email = ? LIMIT 1");
        $stmt->execute(array($email));
        $row = $stmt->fetch();

        if (!$row) {
            $errormsg = "Angegebene E-Mail Adresse ist falsch! <br> Bitte versuchen Sie es erneut.";
            header('location: login.php?errormsg=' . $errormsg);
        } else if (password_verify($password, $row['password'])) {
            $user = new User($row['id'], $row['firstname'], $row['lastname'], $row['email'], $row['password'], $row['role']);
            $_SESSION['user'] = $user;
            header('Location: ../Anfragen/anfragen.php');
        } else {
            $errormsg = "Angegebenes Passwort ist fehlerhaft! <br> Bitte versuchen Sie es erneut.";
            header('location: login.php?errormsg=' . $errormsg);
        }
    }
    ?>

    <!--Bootstrap 4.5 resources-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>