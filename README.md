# SmartRequest

- Dieses Projekt wurde im Rahmen des "WI-Projekt: Systementwicklung" des [Lehrstuhl IIS](https://www.uni-bamberg.de/iis/)
 an der Otto-Friedrich-Universität Bamberg, für die [slidemotif AG](https://www.slidemotif.com/)  erstellt.

- Das Tool soll den Prozess der Anfrageverwaltung und -beantwortung vollkommen integriert darstellen, ohne auf E-Mail Programme
oder andere Systeme zugreifen zu müssen. Zudem lassen sich Kontakte, CVs und Textbausteine verwalten. Letztere erleichtern
die Generierung von Antwort-Emails. 

- SmartRequest bietet weiterhin ein integriertes Reporting, wo verschiedenste für die Verwaltung relevante Kennzahlen 
in Grafiken dargestellt sind, die sich mit der Nutzung des Tools dynamisch anpassen, wie zum Beispiel die durchschnittliche Durchlaufzeit
von Anfragen.

## Allgemeine Hinweise

- Ein zentrales E-Mail-Postfach, an das die Anfragen gerichtet sind, muss im Tool eingebunden sein. (Nutzername und Passwort benötigt)

- Für die Entwicklung wurde ein Gmail-Postfach mit der Adresse "wip.ss20.g7@gmail.com" angelegt.


 
- Genauere Nutzungshinweise entnehmen Sie bitte dem Nutzerhandbuch.

### Genutzte Technologien

- HTML5 & CSS3

- JavaScript

- PHP (v7.4)

### Genutzte Bibliotheken/Frameworks/etc.

- [Bootstrap v4.5.0](https://getbootstrap.com/)

- [JQuery](https://jquery.com/)

- [PHP IMAP](https://github.com/barbushin/php-imap)

- [PHP Mailer](https://github.com/PHPMailer/PHPMailer)

- [Composer](https://getcomposer.org/)

- [CKEditor](https://ckeditor.com/ckeditor-4/)